#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy
import random
import math
import scipy.stats
import scipy.special
import time

# CALPHA = 1  # alpha = 0.6826
CALPHA = 1.96  # alpha = 0.95


def montecarlo(fct, nbIterations):
    espApprox = 0.
    for n in range(0, nbIterations):
        xn = fct(random.random())
        espApprox = espApprox + xn
    espApprox = espApprox / nbIterations
    return espApprox


def montecarloTCL(fct, precision):
    n = 2
    xn = [fct(random.random()), fct(random.random())]
    espApprox = (1./n) * sum(xn)
    ecartype = numpy.sqrt((1./(n-1)) * sum([(xn[i] - espApprox)*(xn[i] - espApprox) for i in range(n)]))
    while (CALPHA * ecartype / numpy.sqrt(n) > precision):
        xn.append(fct(random.random()))
        n = n + 1
        espApprox = (1./n) * sum(xn)
        ecartype = numpy.sqrt((1. / (n - 1)) * sum([(xn[i] - espApprox) * (xn[i] - espApprox) for i in range(n)]))
        # print(CALPHA * ecartype / numpy.sqrt(n))
    print(n)
    return espApprox


def montecarlofluct(fct, precision, d):
# où d est le nombre d'étapes sur lesquelles on évalue les fluctuations
    n = 1
    moyXn = [fct(random.random())]
    for i in range(d):
        moyXn.append( ( moyXn[i]*n+fct(random.random()) )/(n+1) )
        n = n+1

    while (max([abs(moyXn[0]-moyXn[k]) for k in range(1, d+1)]) > precision):
        moyXn.append( ( moyXn[d]*n+fct(random.random()) )/(n+1) )
        del moyXn[0]
        n = n+1
    print("nombre d'itérations :")
    print(n)
    return moyXn[d]


def montecarloBienaymeTchebychev(fct, precision, confiance):
    alpha = 1 - confiance
    n = 2
    xn = [fct(random.random()), fct(random.random())]
    espApprox = (1./n) * sum(xn)
    ecartype = numpy.sqrt((1./(n-1)) * sum([(xn[i] - espApprox)*(xn[i] - espApprox) for i in range(n)]))
    r = ecartype / numpy.sqrt(n * alpha)
    while r > precision:
        xn.append(fct(random.random()))
        n = n + 1
        espApprox = (1./n) * sum(xn)
        ecartype = numpy.sqrt((1. / (n - 1)) * sum([(xn[i] - espApprox) * (xn[i] - espApprox) for i in range(n)]))
        r = ecartype / numpy.sqrt(n * alpha)
    print("nombre d'itérations :")
    print(n)
    return espApprox


# main
print("Intégrale de sin x entre 0 et 1 :\n")
print("estimation avec 100 000 itérations :")
start_time = time.time()
print(montecarlo(math.sin, 100000))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("avec le Thm central limite :")
start_time = time.time()
print(montecarloTCL(math.sin, 0.005))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("avec les fluctuations :")
start_time = time.time()
print(montecarlofluct(math.sin, 0.005, 30))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("avec Bienaymé Tchebychev :")
start_time = time.time()
print(montecarloBienaymeTchebychev(math.sin, 0.005, 0.65))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("valeur attendue :")
print(1-math.cos(1.))

print("\n\nIntégrale de Gauss entre 0 et 1 :\n")
print(montecarlofluct(lambda x: 1/numpy.sqrt(2*numpy.pi) * numpy.exp((-x*x)/2), 0.005, 30))
print("valeur attendue :")
scipy.stats.norm(0, 1)
print(scipy.stats.norm(0, 1).cdf(1) - scipy.stats.norm(0, 1).cdf(0))

print("\n\nIntégrale de sinus cardinal entre 0 et 1 :\n")
print(montecarlofluct(lambda x: numpy.sin(x) / x, 0.005, 30))
print("valeur attendue :")
print(scipy.special.sici(1)[0])

print("\n\nEtude de d avec les fluctuations\n")

print("d = 30\n")
start_time = time.time()
print(montecarlofluct(math.sin, 0.005, 30))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("d = 40\n")
start_time = time.time()
print(montecarlofluct(math.sin, 0.005, 40))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("d = 70\n")
start_time = time.time()
print(montecarlofluct(math.sin, 0.005, 70))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("d = 100\n")
start_time = time.time()
print(montecarlofluct(math.sin, 0.005, 100))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("d = 250\n")
start_time = time.time()
print(montecarlofluct(math.sin, 0.005, 250))
print("--- %s seconds ---\n" % (time.time() - start_time))
