\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[colorlinks = true,
			linkcolor = blue]{hyperref}
\usepackage{amsfonts, amsmath, amssymb, minted, graphicx}

%opening
\title{Projet de statistiques et recherche opérationnelle : méthode de Monte-Carlo}
\author{Robin \textsc{Langlois} et Nathan \textsc{Flambard}}

\begin{document}

\maketitle

\begin{abstract}
Projet de GM3 de la deuxième vague, Stats-RO projet 10. Le but est d'étudier la méthode de Monte-Carlo ainsi que ses applications.
\end{abstract}

\newpage

\tableofcontents

\newpage

\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}

Depuis des siècles, le hasard a toujours fasciné. D'abord étudié principalement pour les jeux de hasard, ce qui a donné naissance à la théorie des jeux, c'est principalement au XVIIème siècle, notamment avec Pascal et Fermat, que naît la théorie des probabilités. Aujourd'hui encore, des résultats qui semblent ``organiser'' le hasard, comme la loi des grands nombres, le théorème central limite ou l'étude des marches aléatoires, laissent songeur sur la véritable nature de l'aléatoire.

Notre projet va étudier cet aspect de structure du hasard pour permettre d'approximer des grandeurs. En effet, grâce à la simulation numérique, on peut étudier un grand nombre de données et voir apparaître plus facilement les phénomènes étudiés. Cela était impensable avant car il était difficile d'étudier l'aléatoire et surtout à grande échelle. Désormais, l'informatique permet de donner quelque chose de relativement proche de l'aléatoire.

Nous allons maintenant présenter plus en détail la méthode que nous allons utiliser, qui est la méthode de Monte-Carlo.

\newpage
\section{Problème}

\subsection{Exposé du problème à résoudre}

La méthode de Monte-Carlo est une méthode pour estimer l'espérance d'une variable aléatoire à partir de ses observations. Elle se base principalement sur la loi des grands nombres, qui dit entre autres que lorsque le nombre d'observations devient grand, la moyenne empirique converge vers l'espérance. 

Pour un échantillon de $n$ observations $\{X_1; ... ; X_n\}$ d'une variable aléatoire $X$, sa moyenne empirique $\overline{X_{(n)}}$ est définie par :
$$ \overline{X_{(n)}} = \frac{X_1 + ... + X_n}{n} $$

Cette méthode est relativement efficace pour estimer une espérance. Cependant, elle possède un défaut : dans la pratique, on ne peut faire une infinité d'observations pour obtenir la valeur exacte de l'espérance et il faut donc faire un nombre fini $n$ d'observations, en général par simulation numérique. Il faut décider à quel moment on arrête les mesures, et donc trouver un critère d'arrêt pour $n$. 

\subsection{Historique et applications}

Cette méthode désigne une famille de méthodes où l'on utilise des phéno\-mènes aléatoires pour approcher des grandeurs numériques. Le nom vient de Nicholas Metropolis qui l'a inventé en 1947 et publié en 1949, en référence aux jeux de hasard que l'on peut trouver à Monte-Carlo, un quartier de Monaco.

Ces méthodes ont été largement développées pendant la Seconde Guerre mondiale, notamment par John von Neumann et Stanislaw Ulam, dans le cadre des recherches sur la bombe atomique. Elles ont servies notamment à résoudre des équations aux dérivées partielles.

Aujourd'hui, on les utilise principalement pour approcher des intégrales difficiles à calculer ou dans des espaces de dimension supérieure à 1. Elles permettent aussi en physique des particules d'estimer la forme d'un signal, ou encore en finances pour étudier le risque. On s'en sert également en intelligence artificielle, par exemple pour évaluer un coup au jeu de go ou aux échecs.

\subsection{Résolution théorique}

Afin de déterminer le critère d'arrêt, nous avons utilisé trois différentes méthodes: la première utilise l'inégalité de Bienaymé Tchebychev, la deuxième le théorème central limite et la dernière étudie les fluctuations de $\overline{X_{(n)}}$.

\subsubsection{Bienaymé Tchebychev}
L'inégalité de Bienaymé Tchebychev est la suivante : 
Pour $X$ une variable aléatoire de carré intégrable, on a :
$$P(\left| X - E(X)\right| \geqslant r ) \leqslant \frac{V(X)}{r^2} \: \: \: \: \forall r > 0$$

Soit $S_{N} = \frac{1}{N} \times \sum_{n = 1}^{N} X_{n} $ alors $S_{N}$ est une variable aléatoire. On pose $m = E(S_{N})$ et $\sigma^2 = V(X)$ alors on a $V(S_{N}) = \frac{\sigma^2}{N}$. D'où $P(\left| S_{N} - m \right| \geqslant r) \leqslant \frac{\sigma^2}{Nr^2}$. On a donc $P(m \in [S_{N} - r ; S_{N} + r])\geqslant 1 - \frac{\sigma^2}{Nr^2}$. Donc pour avoir une approximation de $m$ à $r$ près avec un niveau de confiance $1-\alpha$ on doit s'arrêter quand $\frac{\sigma^2}{N\alpha} \leqslant r^2$.


\subsubsection{Théorème central limite}

Le thèorème central limite nous dit que: 
Soient $X_1 , X_2,\dots X_n $ des variables aléatoires suivant la même loi d'espérance $\mu$ et d'écart-type $\sigma \neq 0$. Soit $S_n = \sum_{k=1}^{n} X_k$ alors on a $E(S_n) = n \mu$, $V(S_n) = n\sigma^2$ et pour $n$ assez grand on a $Sn \approx S \leadsto N(n\mu, n\sigma^ 2) $. On en déduit donc :
$$P(\left|\frac{S_n}{n} - \mu \right| < r) = P( n\mu - nr < S_n < n\mu + nr) \approx P(\left|Z\right| < \frac{nr}{\sqrt{n}\sigma})$$
où $Z \leadsto N(0,1)$. on a donc :
$$P(\left|\frac{S_n}{n} - \mu \right| < r) \approx P(\left|Z\right|< \frac{\sqrt{n}r}{\sigma})$$
Donc pour avoir une estimation de $\mu$ à $r$ près à un niveau de confiance $\alpha$ on doit avoir $P(|Z| <\frac{\sqrt{n}r}{\sigma}) \geqslant \alpha$ donc si on pose $C_{\alpha}$ tel que $P(|Z|< C_{\alpha})= \alpha$ alors on doit s'arrêter quand $\frac{\sqrt{n}r}{\sigma} > C_{\alpha} \Leftrightarrow r > \frac{\sigma C_{\alpha}}{\sqrt{n}}$.


\subsubsection{Étude des fluctuations}

Le principe de cette méthode est d'évaluer les fluctuations suivantes :
$$\displaystyle \sup_{1\leqslant k \leqslant d} \left|\overline{X_{(n)}} - \overline{X_{(n+k)}} \right|$$

Il faut donc poser un $d$ suffisamment grand. On doit alors continuer les itérations jusqu'à ce que la moyenne se stabilise, c'est-à-dire jusqu'à ce que les fluctuations des moyennes soient inférieures à une certaine précision que nous nous serons donnée. Ce critère nécessite donc de fixer deux valeurs : $d$ et la précision voulue.

\newpage
\section{Algorithmes}
Maintenant que les différents critères d'arrêts sont établis, il faut établir les algorithmes permettant d'obtenir l'approximation voulue. Ici, on s'intéressera à approximer pour une fonction $f$ donnée en entrée la valeur de $\int_{0}^{1}f(x)dx$. Nous avons choisi d'implémenter les algorithmes en Python afin de pouvoir travailler facilement avec des listes (stockage des $X_k$/$\overline{X_{(n)}}$, calcul de sommes...) et de pouvoir vérifier nos résultats avec des librairies telles que Numpy et Scipy.  

\subsection{Méthode naïve}
Tout d'abord, nous avons implémenté une méthode calculant simplement la moyenne empirique $\overline{X_{(n)}}$ où $X = f(Y)$ avec $Y\leadsto U([0;1])$ pour un $n$ donné. On obtient la fonction suivante :
\begin{minted}{python}
 def montecarlo(fct, nbIterations):
    espApprox = 0.
    for n in range(0, nbIterations):
        xn = fct(random.random())
        espApprox = espApprox + xn
    espApprox = espApprox / nbIterations
    return espApprox
\end{minted}

\subsection{Avec l'inégalité de Bienaymé Tchebychev }
L'inégalité nous a permis de déterminer, pour une précision $r$ et un niveau de confiance $\alpha$, la condition d'arrêt suivante : $\frac{\sigma}{\sqrt{N\alpha}} \leqslant r$. Il est donc nécessaire d'estimer la variance $\sigma^2$ à partir des observations $X_k \; \; \; k \in 1,\dots,N$. Pour cela, on utilise une estimation de la variance non biaisée donnée par :
$$\sigma^2 \approx  \frac{\sum_{k=1}^{N} (X_{k}-\overline{X_{(N)}})^2}{N-1} $$

\begin{minted}[breaklines]{python}
 
def montecarloBienaymeTchebychev(fct, precision, confiance):
    alpha = 1 - confiance
    n = 2
    xn = [fct(random.random()), fct(random.random())]
    espApprox = (1./n) * sum(xn)
    ecartype = numpy.sqrt((1./(n-1)) * sum([(xn[i] - espApprox)*(xn[i] - espApprox) for i in range(n)]))
    r = ecartype / numpy.sqrt(n * alpha)
    while r > precision:
        xn.append(fct(random.random()))
        n = n + 1
        espApprox = (1./n) * sum(xn)
        ecartype = numpy.sqrt((1. / (n - 1)) * sum([(xn[i] - espApprox) * (xn[i] - espApprox) for i in range(n)]))
        r = ecartype / numpy.sqrt(n * alpha)
    return espApprox


\end{minted}

\subsection{Avec le théorème central limite}
Le théorème central limite nous donne la condition d'arrêt suivante : $r > \frac{\sigma C_{\alpha}}{\sqrt{n}} $ ici encore on aura besoin d'approximer la variance. Pour $C_{\alpha}$ on utilisera une constante comme suit :
\begin{minted}[breaklines]{python}
CALPHA = 1  # alpha = 0.6826
CALPHA = 1.96  # alpha = 0.95
\end{minted}
On obtient donc la fonction suivante :
\begin{minted}[breaklines]{python}
 def montecarloTCL(fct, precision):
    n = 2
    xn = [fct(random.random()), fct(random.random())]
    espApprox = (1./n) * sum(xn)
    ecartype = numpy.sqrt((1./(n-1)) * sum([(xn[i] - espApprox)*(xn[i] - espApprox) for i in range(n)]))
    while (CALPHA * ecartype / numpy.sqrt(n) > precision):
        xn.append(fct(random.random()))
        n = n + 1
        espApprox = (1./n) * sum(xn)
        ecartype = numpy.sqrt((1. / (n - 1)) * sum([(xn[i] - espApprox) * (xn[i] - espApprox) for i in range(n)]))
    return espApprox

\end{minted}


\subsection{Avec l'étude des fluctuations}

Ici notre condition d'arrêt est : $\displaystyle \sup_{1\leqslant k \leqslant d} \left|\overline{X_{(n)}} - \overline{X_{(n+k)}} \right| < r$. On aura donc besoin de stocker les $d+1$ dernières valeurs de $\overline{X_{(n)}}$ obtenues à chaque itération. On les stockera donc dans la liste nommée \mintinline{python}{moyXn} dans le code. D'où la fonction :  

\begin{minted}[breaklines]{python}
 
def montecarlofluct(fct, precision, d):
# où d est le nombre d'étapes sur lesquelles on évalue les fluctuations
    n = 1
    moyXn = [fct(random.random())]
    for i in range(d):
        moyXn.append( ( moyXn[i]*n+fct(random.random()) )/(n+1) )
        n = n+1

    while (max([abs(moyXn[0]-moyXn[k]) for k in range(1, d+1)]) > precision):
        moyXn.append( ( moyXn[d]*n+fct(random.random()) )/(n+1) )
        del moyXn[0]
        n = n+1
    return moyXn[d]

\end{minted}





\newpage
\section{Analyse des résultats}

Nous allons tout d'abord comparer les différentes méthodes par rapport au temps d'exécution et au nombre d'itérations.
Dans un premier temps, nous avons testé les trois différentes méthodes (et la méthode naïve) pour calculer l'intégrale $\int_0^1 sin\, x \, dx$, puis nous avons calculé avec la méthode des fluctuations deux intégrales que l'on ne sait pas calculer explicitement : l'intégrale de Gauss (probabilité qu'une variable aléatoire suivant la loi normale centrée réduite soit entre 0 et 1 : $\int_0^1 \frac{1}{\sqrt{2\pi}} e^\frac{-x^2}{2} dx$) et l'intégrale $\int_0^1 \frac{sin\, x}{x} dx$. Voici un exemple d'exécution :\\

\includegraphics[scale=0.5]{images/programme_complet_avec_temps.png}\\ \\

Pour la méthode naïve, on obtient évidemment une grande précision lorsqu'on demande un grand nombre d'itérations. Il n'y a aucun calcul à faire donc les itérations sont rapides.

Les trois prochaines méthodes demandent une précision de 0,005.

La méthode du théorème central limite est celle des trois suivantes qui demande le plus d'itérations : elle en fait près de 10 000 pour s'arrêter. De plus, les itérations demandent beaucoup de temps car elles demandent de gros calculs (notamment le calcul de l'écart-type).

La méthode des fluctuations ne fait que 229 itérations (ici $d$ = 30, on discutera plus tard suivant ses valeurs). Les itérations sont un peu coûteuses (calcul de somme) mais moins que pour le théorème central limite. On a donc naturellement un temps d'exécution beaucoup plus faible que pour le TCL. Cependant, la valeur est par conséquent un peu moins précise (on est moins précis de 0,004 sur cet exemple).

La méthode de Bienaymé-Tchebychev fait ici un peu moins de 7000 itérations. Les itérations sont un peu moins coûteuses que pour le théorème central limite, et on en fait un peu moins. On est ici à une précision comparable à celle du théorème central limite. 

Dans chaque cas, nous sommes assez proches de la valeur exacte (qui est ici $1 - cos(1)$, soit environ $0.4597$ comme on peut le voir).

Au niveau de la précision, on demande une précision de 0,005, et on peut voir que le théorème central limite nous donne une valeur écartée d'environ 0,0067, ce qui est moins précis que ce que nous avions demandé. Il faut savoir qu'un intervalle de confiance est associé à cette valeur, et nous ne sommes donc pas assurés d'avoir une précision de plus ou moins 0,005. Ici, le seuil de confiance était de 95\%. Bienaymé-Tchebychev nous a donné un écart de moins de 0,003, ce qui est légèrement mieux que ce que nous avions demandé. Il y a aussi un niveau de confiance associé à cette grandeur : ici, on demandait 65\%.

Pour les deux autres intégrales, nous avons choisi de n'utiliser qu'une seule méthode pour alléger les tests. Nous observons que l'approximation est plutôt correcte et que nos valeurs sont proches, même en utilisant la méthode des fluctuations avec un nombre d'itérations plutôt faible (inférieur à 100).\\

Nous allons maintenant étudier plus en détail la méthode des fluctuations et son évolution suivant différentes valeurs de $d$.\\

\includegraphics[scale=0.5]{images/etude_d.png}\\

Nous avons ici testé sur la première intégrale étudiée ($\int_0^1 sin\, x \, dx$) avec les valeurs $d = 30, 40, 70, 100$ et $250$. Comme on aurait pu le prévoir, le nombre d'itérations et le temps d'exécution augmentent avec la valeur de $d$. Bien sûr, si l'on augmente la valeur de $d$ significativement, la précision s'en trouvera améliorée.\\

Pour finir, il existe un moyen d'améliorer la précision beaucoup plus efficacement qu'en augmentant le nombre d'itérations. On peut améliorer notre convergence en effectuant plusieurs tirages et en en faisant la moyenne, plutôt que de beaucoup augmenter notre nombre d'itérations. En effet, nous avons vu notamment avec le théorème de Bienaymé-Tchebychev et le théorème central limite que le résultat converge en $\frac{1}{\sqrt{n}}$ où $n$ est le nombre d'itérations. Il est donc plus intéressant par exemple de faire la moyenne de deux tirages avec le même nombre d'itérations plutôt que de multiplier par 10 ce nombre d'itérations.

Nous avons testé, toujours avec l'intégrale $\int_0^1 sin\, x \, dx$ de faire un tirage à 5 000 itérations, un à 10 000, et de comparer la moyenne des deux à un tirage à 100 000 itérations.\\

\includegraphics[scale=0.5]{images/comparaison_jgc.png}\\

Pour un total de 15 000 itérations (et un calcul de moyenne) au lieu de 100 000, et en 10 fois moins de temps, nous avons une valeur approchée rivalisant avec celle des 100 000 itérations (écart de 0,003). Nous avons fait la moyenne de seulement 2 tirages mais nous pourrions faire la moyenne de plus de tirages pour encore améliorer notre vitesse de convergence.

\newpage
\section*{Conclusion}
\addcontentsline{toc}{section}{Conclusion}
Nous avons donc pu voir durant ce projet les enjeux de la méthode de Monte-Carlo. Cette méthode, utilisant le hasard, n'est de fait pas exacte : mais elle permet d'approximer avec une grande précision des grandeurs que nous ne pouvons pas toujours calculer. De plus, le hasard que nous utilisons ici est du ``faux'' hasard : les ordinateurs étant des systèmes déterministes, ils ne peuvent pas créer de hasard et font donc ce qu'on appelle du pseudo-aléatoire : ils utilisent certains paramètres comme l'heure, et leur appliquent des calculs complexes ce qui fait passer cela pour du hasard. On pourrait donc se demander si la méthode fonctionnerait mieux avec du ``véritable'' hasard. 

Mais cela soulève une question fondamentale : qu'est-ce que le véritable hasard, et existe-t-il ? Depuis des temps immémoriaux, les humains, faisant progresser la civilisation et la science, parviennent à expliquer de plus en plus de choses, notamment des phénomènes physiques. Ce qu'on pouvait croire aléatoire il y a plusieurs siècles ne l'est plus forcément aujourd'hui. Aujourd'hui, on ne peut toujours pas tout expliquer, mais des physiciens comme Laplace par exemple pensaient qu'un jour, nous connaîtrons toutes les lois qui régissent notre univers, et nous pourrons donc tout expliquer, voire tout prédire à partir d'un état donné. Tous les physiciens ne sont cependant pas d'accord sur cette question. Cela a été notamment mis en cause avec l'arrivée de la mécanique quantique, et du principe d'incertitude d'Heisenberg, ce qui a provoqué un grand bouleversement dans la physique. Mais aujourd'hui encore, on ne sait répondre à cette question avec certitude.

\newpage
\section*{Bibliographie}
\addcontentsline{toc}{section}{Bibliographie}

\renewcommand\refname{}
\begin{thebibliography}{4}
\bibitem{wikipedia}
    Wikipédia,
    \emph{Méthode de Monte-Carlo}.\\
    \url{https://fr.wikipedia.org/wiki/M\%C3\%A9thode_de_Monte-Carlo}
	  
\bibitem{coursMCPopier}
    Alexandre \textsc{Popier}, Université du Maine, Le Mans,
    \emph{Méthode de Monte Carlo}.\\
    \url{http://perso.univ-lemans.fr/~apopier/enseignement/cours_div/slides_MC.pdf} \\

\bibitem{mcoursMCKharroubi}
    Idris \textsc{Kharroubi}, Sorbonne Université,
    \emph{Méthodes de Monte Carlo}. \\
    \url{http://www.lpsm.paris/pageperso/kharroubii/teaching/MoneCarloIFMA.pdf} \\
    
\bibitem{JGC}
    Les conseils avisés de Jean-Guy \textsc{Caputo}, enseignant à l'INSA de Rouen.
\end{thebibliography}

\newpage
\section*{Annexes}
\addcontentsline{toc}{section}{Annexes}

\subsection*{Code complet}
\addcontentsline{toc}{subsection}{Code complet}

\begin{minted}[breaklines]{python}
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy
import random
import math
import scipy.stats
import scipy.special
import time

# CALPHA = 1  # alpha = 0.6826
CALPHA = 1.96  # alpha = 0.95


def montecarlo(fct, nbIterations):
    espApprox = 0.
    for n in range(0, nbIterations):
        xn = fct(random.random())
        espApprox = espApprox + xn
    espApprox = espApprox / nbIterations
    return espApprox


def montecarloTCL(fct, precision):
    n = 2
    xn = [fct(random.random()), fct(random.random())]
    espApprox = (1./n) * sum(xn)
    ecartype = numpy.sqrt((1./(n-1)) * sum([(xn[i] - espApprox)*(xn[i] - espApprox) for i in range(n)]))
    while (CALPHA * ecartype / numpy.sqrt(n) > precision):
        xn.append(fct(random.random()))
        n = n + 1
        espApprox = (1./n) * sum(xn)
        ecartype = numpy.sqrt((1. / (n - 1)) * sum([(xn[i] - espApprox) * (xn[i] - espApprox) for i in range(n)]))
        # print(CALPHA * ecartype / numpy.sqrt(n))
    print(n)
    return espApprox


def montecarlofluct(fct, precision, d):
# où d est le nombre d'étapes sur lesquelles on évalue les fluctuations
    n = 1
    moyXn = [fct(random.random())]
    for i in range(d):
        moyXn.append( ( moyXn[i]*n+fct(random.random()) )/(n+1) )
        n = n+1

    while (max([abs(moyXn[0]-moyXn[k]) for k in range(1, d+1)]) > precision):
        moyXn.append( ( moyXn[d]*n+fct(random.random()) )/(n+1) )
        del moyXn[0]
        n = n+1
    print("nombre d'itérations :")
    print(n)
    return moyXn[d]


def montecarloBienaymeTchebychev(fct, precision, confiance):
    alpha = 1 - confiance
    n = 2
    xn = [fct(random.random()), fct(random.random())]
    espApprox = (1./n) * sum(xn)
    ecartype = numpy.sqrt((1./(n-1)) * sum([(xn[i] - espApprox)*(xn[i] - espApprox) for i in range(n)]))
    r = ecartype / numpy.sqrt(n * alpha)
    while r > precision:
        xn.append(fct(random.random()))
        n = n + 1
        espApprox = (1./n) * sum(xn)
        ecartype = numpy.sqrt((1. / (n - 1)) * sum([(xn[i] - espApprox) * (xn[i] - espApprox) for i in range(n)]))
        r = ecartype / numpy.sqrt(n * alpha)
    print("nombre d'itérations :")
    print(n)
    return espApprox


# main (tests)
print("Intégrale de sin x entre 0 et 1 :\n")
print("estimation avec 100 000 itérations :")
start_time = time.time()
print(montecarlo(math.sin, 100000))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("avec le Thm central limite :")
start_time = time.time()
print(montecarloTCL(math.sin, 0.005))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("avec les fluctuations :")
start_time = time.time()
print(montecarlofluct(math.sin, 0.005, 30))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("avec Bienaymé Tchebychev :")
start_time = time.time()
print(montecarloBienaymeTchebychev(math.sin, 0.005, 0.65))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("valeur attendue :")
print(1-math.cos(1.))

print("\n\nIntégrale de Gauss entre 0 et 1 :\n")
print(montecarlofluct(lambda x: 1/numpy.sqrt(2*numpy.pi) * numpy.exp((-x*x)/2), 0.005, 30))
print("valeur attendue :")
scipy.stats.norm(0, 1)
print(scipy.stats.norm(0, 1).cdf(1) - scipy.stats.norm(0, 1).cdf(0))

print("\n\nIntégrale de sinus cardinal entre 0 et 1 :\n")
print(montecarlofluct(lambda x: numpy.sin(x) / x, 0.005, 30))
print("valeur attendue :")
print(scipy.special.sici(1)[0])

print("\n\nEtude de d avec les fluctuations\n")

print("d = 30\n")
start_time = time.time()
print(montecarlofluct(math.sin, 0.005, 30))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("d = 40\n")
start_time = time.time()
print(montecarlofluct(math.sin, 0.005, 40))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("d = 70\n")
start_time = time.time()
print(montecarlofluct(math.sin, 0.005, 70))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("d = 100\n")
start_time = time.time()
print(montecarlofluct(math.sin, 0.005, 100))
print("--- %s seconds ---\n" % (time.time() - start_time))

print("d = 250\n")
start_time = time.time()
print(montecarlofluct(math.sin, 0.005, 250))
print("--- %s seconds ---\n" % (time.time() - start_time))
\end{minted}


\end{document}
